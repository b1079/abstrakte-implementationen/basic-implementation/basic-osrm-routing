package com.kahlkepartner.osrmrouting.api;

import com.kahlkepartner.osrmrouting.model.data.Coordinates;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RouteServiceTest {
    @Autowired
    RouteService routeService;

    @Test
    void calculateRoute() {
        var res = routeService.calculateRoute(new Coordinates("49.7967", "8.1706"),
                new Coordinates("49.8042", "8.1939"));
        System.out.println(res);
        assertNotNull(res);
    }
}
