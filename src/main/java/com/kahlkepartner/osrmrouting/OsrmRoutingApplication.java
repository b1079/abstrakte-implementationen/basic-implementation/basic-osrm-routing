package com.kahlkepartner.osrmrouting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OsrmRoutingApplication {

    public static void main(String[] args) {
        SpringApplication.run(OsrmRoutingApplication.class, args);
    }

}
