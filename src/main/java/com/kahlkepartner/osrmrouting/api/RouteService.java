package com.kahlkepartner.osrmrouting.api;

import com.kahlkepartner.osrmrouting.model.RouteServiceResponse;
import com.kahlkepartner.osrmrouting.model.data.Coordinates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
@ConditionalOnExpression("${osrm.enabled:true}")
/**
 * Route calculation service for Osrm
 */
public class RouteService {
    @Value("${osrm.endpoint.url}")
    String url;
    public RouteServiceResponse calculateRoute(Coordinates from, Coordinates to) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.ALL));
        RestTemplate restTemplate = new RestTemplate();

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        builder.pathSegment("route", "v1", "driving", from + ";" + to);
        builder.queryParam("geometries", "geojson");
        builder.queryParam("overview", "full");

        HttpEntity<?> request = new HttpEntity<>(headers);

        HttpEntity<RouteServiceResponse> response = restTemplate.exchange(
                builder.build().toUriString(),
                HttpMethod.GET,
                request,
                RouteServiceResponse.class
        );

        return response.getBody();
    }


}
