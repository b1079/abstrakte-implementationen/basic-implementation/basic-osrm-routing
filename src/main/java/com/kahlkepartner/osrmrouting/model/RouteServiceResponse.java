package com.kahlkepartner.osrmrouting.model;

import com.kahlkepartner.osrmrouting.model.data.Route;
import com.kahlkepartner.osrmrouting.model.data.Waypoint;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
/**
 * Response from the osrm route service
 */
public class RouteServiceResponse {
    List<Waypoint> waypoints;
    List<Route> routes;
    String code;
}
