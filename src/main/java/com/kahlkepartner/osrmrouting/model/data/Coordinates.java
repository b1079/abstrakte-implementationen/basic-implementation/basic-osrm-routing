package com.kahlkepartner.osrmrouting.model.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Coordinates {
    String longitude;
    String latitude;

    @Override
    public String toString() {
        return String.format("%s,%s", longitude, latitude);
    }
}
