package com.kahlkepartner.osrmrouting.model.data;

import lombok.Data;

@Data
public class Waypoint {
    public String name;
    public String hint;
    public String[] location;
    public double distance;

}
