package com.kahlkepartner.osrmrouting.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import model.geometry.GeoJSONLineString;

import java.util.List;

@Data
public class Route {
    double distance;
    double duration;
    GeoJSONLineString geometry;
    double weight;
    @JsonProperty("weight_name")
    String weightName;
    List<RouteLeg> legs;
}
