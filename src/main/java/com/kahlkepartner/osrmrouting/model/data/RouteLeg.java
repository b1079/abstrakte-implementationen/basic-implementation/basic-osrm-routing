package com.kahlkepartner.osrmrouting.model.data;

import lombok.Data;

import java.util.List;

@Data
public class RouteLeg {
    double distance;
    double duration;
    double weight;
    List<RouteStep> steps;
    Annotation annotation;
}
