package com.kahlkepartner.osrmrouting.model.data;

import lombok.Data;

@Data
public class RouteStep {
    double distance;
    double duration;
    String geometry;
    double weight;
    String name;
    // Todo missing maneuver and intersections

}
