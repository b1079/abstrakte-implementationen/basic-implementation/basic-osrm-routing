package com.kahlkepartner.osrmrouting.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Annotation {
    List<Integer> distance;
    List<Integer> duration;
    @JsonProperty("datasources")
    List<Integer> dataSources;
    List<Long> nodes;
    List<Long> weight;
    // todo add metadata
}
